import java.util.Scanner;

public class RnaTranscription {
	

	public String transcribe(String dnaStrand) {
		
		String newStrand = "";
		for (int x = 0; x < dnaStrand.length(); x++) {
			switch (dnaStrand.charAt(x)) {
			case 'G':
				newStrand += 'C';
				break;
			case 'C':
				newStrand += 'G';
				break;
			case 'T':
				newStrand += 'A';
				break;
			case 'A':
				newStrand += 'U';
				break;
			}

		}
		return newStrand;
	}

}

import java.util.Scanner;
public class RnaMain {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		RnaTranscription RNA = new RnaTranscription();
		System.out.println("Please enter a DNA strand to have it transcribed");
		String dnaStrand = input.next();
		
		System.out.println("Here is your transcribed strand: " + RNA.transcribe(dnaStrand));
		

	}

}
